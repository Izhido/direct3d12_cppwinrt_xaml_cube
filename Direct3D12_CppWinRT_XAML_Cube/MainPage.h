﻿#pragma once

#include "MainPage.g.h"

struct ModelViewProjectionConstantBuffer
{
	DirectX::XMFLOAT4X4 model;
	DirectX::XMFLOAT4X4 view;
	DirectX::XMFLOAT4X4 projection;
};

static const int frameCount = 3;

namespace winrt::Direct3D12_CppWinRT_XAML_Cube::implementation
{
    struct MainPage : MainPageT<MainPage>
    {
		std::vector<byte> vertexShaderBuffer;
		std::vector<byte> pixelShaderBuffer;
		winrt::com_ptr<IDXGIFactory4> dxgiFactory;
		winrt::com_ptr<ID3D12Device> d3dDevice;
		winrt::com_ptr<ID3D12CommandQueue> commandQueue;
		winrt::com_ptr<ID3D12DescriptorHeap> rtvHeap;
		UINT rtvDescriptorSize;
		winrt::com_ptr<ID3D12DescriptorHeap> dsvHeap;
		winrt::com_ptr<ID3D12CommandAllocator> commandAllocators[frameCount];
		winrt::com_ptr<ID3D12Fence> fence;
		UINT64 fenceValues[frameCount];
		HANDLE fenceEvent;
		winrt::com_ptr<IDXGISwapChain3> swapChain;
		UINT currentFrame;
		winrt::com_ptr<ID3D12Resource> renderTargets[frameCount];
		winrt::com_ptr<ID3D12Resource> depthStencil;
		D3D12_VIEWPORT viewport;
		winrt::com_ptr<ID3D12RootSignature> rootSignature;
		winrt::com_ptr<ID3D12PipelineState> pipelineState;
		winrt::com_ptr<ID3D12GraphicsCommandList> commandList;
		winrt::com_ptr<ID3D12Resource> vertexBuffer;
		winrt::com_ptr<ID3D12DescriptorHeap> cbvHeap;
		winrt::com_ptr<ID3D12Resource> indexBuffer;
		winrt::com_ptr<ID3D12Resource> constantBuffer;
		UINT cbvDescriptorSize;
		UINT8* mappedConstantBuffer = nullptr;
		D3D12_VERTEX_BUFFER_VIEW vertexBufferView;
		D3D12_INDEX_BUFFER_VIEW indexBufferView;
		D3D12_RECT scissorRect;
		ModelViewProjectionConstantBuffer constantBufferData;
		Windows::Foundation::IAsyncAction renderLoopWorker;
		std::mutex renderLoopMutex;
		bool deviceRemoved = true;
		float angle = 0;
		LARGE_INTEGER previousTime = { };
		
		MainPage();

        int32_t MyProperty();
        void MyProperty(int32_t value);

        void ClickHandler(Windows::Foundation::IInspectable const& sender, Windows::UI::Xaml::RoutedEventArgs const& args);

		void OpenVertexShaderFile(Windows::Storage::StorageFolder const& folder);
		void ReadVertexShaderFile(Windows::Storage::StorageFile const& file, Windows::Storage::StorageFolder const& folder);
		void OpenPixelShaderFile(Windows::Storage::StorageFolder const& folder);
		void ReadPixelShaderFile(Windows::Storage::StorageFile const& file);
		void InitializeDevice();
		void CreateDevice();
		void RemoveDevice();
		void swapChainPanel_SizeChanged(winrt::Windows::Foundation::IInspectable const&, winrt::Windows::UI::Xaml::SizeChangedEventArgs const&);
		void swapChainPanel_CompositionScaleChanged(winrt::Windows::UI::Xaml::Controls::SwapChainPanel const&, winrt::Windows::Foundation::IInspectable const&);
		void DpiChanged(Windows::Graphics::Display::DisplayInformation const&, IInspectable const&);
		void OrientationChanged(Windows::Graphics::Display::DisplayInformation const&, IInspectable const&);
		void DisplayContentsInvalidated(Windows::Graphics::Display::DisplayInformation const&, IInspectable const&);
		void RenderLoop(Windows::Foundation::IAsyncAction const&);
		bool Update();
		void Render();
		void WaitForGPU(int frame, const char* timeoutError);
		void UploadBufferToGPU(D3D12_SUBRESOURCE_DATA data, size_t size, winrt::com_ptr<ID3D12Resource> const& buffer, winrt::com_ptr<ID3D12Resource> const& bufferUpload, D3D12_RESOURCE_DESC const& descriptor);
		void CleanupFrameData();
		void CreateOutputSize(float width, float height, Windows::Graphics::Display::DisplayInformation const& displayInformation, Windows::Foundation::Size& outputSize);
		void CreateRotation(Windows::Graphics::Display::DisplayInformation const& displayInformation, DXGI_MODE_ROTATION& rotation);
		void CreateRenderTargetSize(DXGI_MODE_ROTATION& rotation, Windows::Foundation::Size const& size, Windows::Foundation::Size& renderTargetSize);
		void CreateOrientationTransform3D(DXGI_MODE_ROTATION rotation, DirectX::XMFLOAT4X4& orientationTransform3D);
		void UpdateSwapChainTransform(DXGI_MODE_ROTATION rotation, float compositionScaleX, float compositionScaleY);
		void CreateRenderTargetViews();
		void CreateDepthStencilBuffer(UINT width, UINT height);
		void UpdateMatrices(Windows::Foundation::Size const& outputSize, DirectX::XMFLOAT4X4 const& orientationTransform3D);
	};
}

namespace winrt::Direct3D12_CppWinRT_XAML_Cube::factory_implementation
{
    struct MainPage : MainPageT<MainPage, implementation::MainPage>
    {
    };
}
