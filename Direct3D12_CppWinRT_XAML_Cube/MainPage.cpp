﻿#include "pch.h"
#include "MainPage.h"
#include "MainPage.g.cpp"
#include <Windows.UI.Xaml.Media.DXInterop.h>

using namespace winrt;
using namespace DirectX;
using namespace Windows::ApplicationModel;
using namespace Windows::Foundation;
using namespace Windows::Graphics::Display;
using namespace Windows::Storage;
using namespace Windows::Storage::Streams;
using namespace Windows::System::Threading;
using namespace Windows::UI;
using namespace Windows::UI::Core;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Media::Imaging;

struct VertexPositionColor
{
	XMFLOAT3 pos;
	XMFLOAT3 color;
};

static const int alignedConstantBufferSize = (sizeof(ModelViewProjectionConstantBuffer) + 255) & ~255;

static const XMFLOAT4X4 Rotation0
(
	1.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f
);

static const XMFLOAT4X4 Rotation90
(
	0.0f, 1.0f, 0.0f, 0.0f,
	-1.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f
);

static const XMFLOAT4X4 Rotation180
(
	-1.0f, 0.0f, 0.0f, 0.0f,
	0.0f, -1.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f
);

static const XMFLOAT4X4 Rotation270
(
	0.0f, -1.0f, 0.0f, 0.0f,
	1.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f
);

namespace winrt::Direct3D12_CppWinRT_XAML_Cube::implementation
{
    MainPage::MainPage()
    {
        InitializeComponent();
    }

    int32_t MainPage::MyProperty()
    {
        throw hresult_not_implemented();
    }

    void MainPage::MyProperty(int32_t /* value */)
    {
        throw hresult_not_implemented();
    }

    void MainPage::ClickHandler(IInspectable const&, RoutedEventArgs const&)
    {
        myButton().Content(box_value(L"Clicked"));
		if (!deviceRemoved)
		{
			return;
		}
		auto displayInformation = DisplayInformation::GetForCurrentView();
		displayInformation.DpiChanged([this](DisplayInformation const& sender, IInspectable const& e)
			{
				DpiChanged(sender, e);
			});
		displayInformation.OrientationChanged([this](DisplayInformation const& sender, IInspectable const& e)
			{
				OrientationChanged(sender, e);
			});
		displayInformation.DisplayContentsInvalidated([this](DisplayInformation const& sender, IInspectable const& e)
			{
				DisplayContentsInvalidated(sender, e);
			});
		auto folder = Package::Current().InstalledLocation();
		OpenVertexShaderFile(folder);
	}

	void MainPage::OpenVertexShaderFile(StorageFolder const& folder)
	{
		auto task = folder.GetFileAsync(L"SampleVertexShader.cso");
		task.Completed([=](IAsyncOperation<StorageFile> const& operation, AsyncStatus const)
			{
				StorageFile file = operation.get();
				ReadVertexShaderFile(file, folder);
			});
	}

	void MainPage::ReadVertexShaderFile(StorageFile const& file, StorageFolder const& folder)
	{
		auto task = FileIO::ReadBufferAsync(file);
		task.Completed([=](IAsyncOperation<IBuffer> const& operation, AsyncStatus const)
			{
				auto vertexShaderFileBuffer = operation.get();
				vertexShaderBuffer.resize(vertexShaderFileBuffer.Length());
				DataReader::FromBuffer(vertexShaderFileBuffer).ReadBytes(vertexShaderBuffer);
				OpenPixelShaderFile(folder);
			});
	}

	void MainPage::OpenPixelShaderFile(StorageFolder const& folder)
	{
		auto task = folder.GetFileAsync(L"SamplePixelShader.cso");
		task.Completed([=](IAsyncOperation<StorageFile> const& operation, AsyncStatus const)
			{
				StorageFile file = operation.get();
				ReadPixelShaderFile(file);
			});
	}

	void MainPage::ReadPixelShaderFile(StorageFile const& file)
	{
		auto pixelShaderReadTask = FileIO::ReadBufferAsync(file);
		pixelShaderReadTask.Completed([=](IAsyncOperation<IBuffer> const& operation, AsyncStatus const)
			{
				auto pixelShaderFileBuffer = operation.get();
				pixelShaderBuffer.resize(pixelShaderFileBuffer.Length());
				DataReader::FromBuffer(pixelShaderFileBuffer).ReadBytes(pixelShaderBuffer);
				InitializeDevice();
			});
	}

	void MainPage::InitializeDevice()
	{
		swapChainPanel().Dispatcher().RunAsync(CoreDispatcherPriority::High, [=]()
			{
				CreateDevice();
				renderLoopWorker = ThreadPool::RunAsync([=](IAsyncAction const& action)
					{
						RenderLoop(action);
					}, WorkItemPriority::High, WorkItemOptions::TimeSliced);
			});
	}

	void MainPage::CreateDevice()
	{
#if defined(_DEBUG)
		com_ptr<ID3D12Debug> debugController;
		if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController))))
		{
			debugController->EnableDebugLayer();
		}
#endif
		check_hresult(CreateDXGIFactory1(IID_PPV_ARGS(&dxgiFactory)));
		com_ptr<IDXGIAdapter1> adapter = nullptr;
		for (auto i = 0; dxgiFactory->EnumAdapters1(i, adapter.put()) != DXGI_ERROR_NOT_FOUND; i++)
		{
			DXGI_ADAPTER_DESC1 descriptor;
			adapter->GetDesc1(&descriptor);
			if ((descriptor.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) != 0)
			{
				continue;
			}
			if (SUCCEEDED(D3D12CreateDevice(adapter.get(), D3D_FEATURE_LEVEL_11_0, __uuidof(ID3D12Device), nullptr)))
			{
				break;
			}
		}
		if (FAILED(D3D12CreateDevice(adapter.get(), D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&d3dDevice))))
		{
			com_ptr<IDXGIAdapter> warpAdapter;
			check_hresult(dxgiFactory->EnumWarpAdapter(IID_PPV_ARGS(&warpAdapter)));
			check_hresult(D3D12CreateDevice(adapter.get(), D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&d3dDevice)));
		}
		D3D12_COMMAND_QUEUE_DESC queueDesc{ D3D12_COMMAND_LIST_TYPE_DIRECT, 0, D3D12_COMMAND_QUEUE_FLAG_NONE };
		check_hresult(d3dDevice->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(&commandQueue)));
		D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc{ D3D12_DESCRIPTOR_HEAP_TYPE_RTV, frameCount };
		check_hresult(d3dDevice->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&rtvHeap)));
		rtvDescriptorSize = d3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
		D3D12_DESCRIPTOR_HEAP_DESC dsvHeapDesc{ D3D12_DESCRIPTOR_HEAP_TYPE_DSV, 1 };
		check_hresult(d3dDevice->CreateDescriptorHeap(&dsvHeapDesc, IID_PPV_ARGS(&dsvHeap)));
		for (auto i = 0; i < frameCount; i++)
		{
			check_hresult(d3dDevice->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&commandAllocators[i])));
		}
		check_hresult(d3dDevice->CreateFence(fenceValues[0], D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fence)));
		fenceValues[0]++;
		fenceEvent = CreateEventEx(nullptr, FALSE, FALSE, EVENT_ALL_ACCESS);
		auto displayInformation = DisplayInformation::GetForCurrentView();
		Size outputSize;
		CreateOutputSize((float)swapChainPanel().ActualWidth(), (float)swapChainPanel().ActualHeight(), displayInformation, outputSize);
		DXGI_MODE_ROTATION rotation = DXGI_MODE_ROTATION_UNSPECIFIED;
		CreateRotation(displayInformation, rotation);
		Size renderTargetSize;
		CreateRenderTargetSize(rotation, outputSize, renderTargetSize);
		DXGI_SWAP_CHAIN_DESC1 swapChainDesc{ };
		swapChainDesc.Width = lround(renderTargetSize.Width);
		swapChainDesc.Height = lround(renderTargetSize.Height);
		swapChainDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
		swapChainDesc.SampleDesc.Count = 1;
		swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		swapChainDesc.BufferCount = frameCount;
		swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
		swapChainDesc.AlphaMode = DXGI_ALPHA_MODE_IGNORE;
		com_ptr<IDXGISwapChain1> newSwapChain;
		check_hresult(dxgiFactory->CreateSwapChainForComposition(commandQueue.get(), &swapChainDesc, nullptr, newSwapChain.put()));
		newSwapChain.as(swapChain);
		com_ptr<ISwapChainPanelNative> panelNative;
		swapChainPanel().as(panelNative);
		check_hresult(panelNative->SetSwapChain(swapChain.get()));
		XMFLOAT4X4 orientationTransform3D;
		CreateOrientationTransform3D(rotation, orientationTransform3D);
		UpdateSwapChainTransform(rotation, swapChainPanel().CompositionScaleX(), swapChainPanel().CompositionScaleY());
		currentFrame = swapChain->GetCurrentBackBufferIndex();
		CreateRenderTargetViews();
		CreateDepthStencilBuffer(swapChainDesc.Width, swapChainDesc.Height);
		viewport = { 0, 0, renderTargetSize.Width, renderTargetSize.Height, 0, 1 };
		D3D12_DESCRIPTOR_RANGE range{ D3D12_DESCRIPTOR_RANGE_TYPE_CBV };
		range.NumDescriptors = 1;
		range.OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;
		D3D12_ROOT_PARAMETER parameter{ D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE };
		parameter.ShaderVisibility = D3D12_SHADER_VISIBILITY_VERTEX;
		parameter.DescriptorTable.NumDescriptorRanges = 1;
		parameter.DescriptorTable.pDescriptorRanges = &range;
		D3D12_ROOT_SIGNATURE_DESC descRootSignature{ };
		descRootSignature.NumParameters = 1;
		descRootSignature.pParameters = &parameter;
		descRootSignature.Flags = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT | D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS | D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS | D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS | D3D12_ROOT_SIGNATURE_FLAG_DENY_PIXEL_SHADER_ROOT_ACCESS;
		com_ptr<ID3DBlob> signature;
		com_ptr<ID3DBlob> error;
		check_hresult(D3D12SerializeRootSignature(&descRootSignature, D3D_ROOT_SIGNATURE_VERSION_1, signature.put(), error.put()));
		check_hresult(d3dDevice->CreateRootSignature(0, signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(&rootSignature)));
		static const D3D12_INPUT_ELEMENT_DESC inputLayout[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
			{ "COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		};
		D3D12_GRAPHICS_PIPELINE_STATE_DESC state{ };
		state.InputLayout = { inputLayout, _countof(inputLayout) };
		state.pRootSignature = rootSignature.get();
		state.VS.BytecodeLength = vertexShaderBuffer.size();
		state.VS.pShaderBytecode = vertexShaderBuffer.data();
		state.PS.BytecodeLength = pixelShaderBuffer.size();
		state.PS.pShaderBytecode = pixelShaderBuffer.data();
		state.RasterizerState.FillMode = D3D12_FILL_MODE_SOLID;
		state.RasterizerState.CullMode = D3D12_CULL_MODE_BACK;
		state.RasterizerState.FrontCounterClockwise = false;
		state.RasterizerState.DepthBias = D3D12_DEFAULT_DEPTH_BIAS;
		state.RasterizerState.DepthBiasClamp = D3D12_DEFAULT_DEPTH_BIAS_CLAMP;
		state.RasterizerState.SlopeScaledDepthBias = D3D12_DEFAULT_SLOPE_SCALED_DEPTH_BIAS;
		state.RasterizerState.DepthClipEnable = true;
		state.RasterizerState.MultisampleEnable = false;
		state.RasterizerState.AntialiasedLineEnable = false;
		state.RasterizerState.ForcedSampleCount = 0;
		state.RasterizerState.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;
		state.BlendState.AlphaToCoverageEnable = FALSE;
		state.BlendState.IndependentBlendEnable = FALSE;
		const D3D12_RENDER_TARGET_BLEND_DESC defaultRenderTargetBlendDesc =
		{
			FALSE,FALSE,
			D3D12_BLEND_ONE, D3D12_BLEND_ZERO, D3D12_BLEND_OP_ADD,
			D3D12_BLEND_ONE, D3D12_BLEND_ZERO, D3D12_BLEND_OP_ADD,
			D3D12_LOGIC_OP_NOOP,
			D3D12_COLOR_WRITE_ENABLE_ALL,
		};
		for (auto i = 0; i < D3D12_SIMULTANEOUS_RENDER_TARGET_COUNT; i++)
		{
			state.BlendState.RenderTarget[i] = defaultRenderTargetBlendDesc;
		}
		state.DepthStencilState.DepthEnable = true;
		state.DepthStencilState.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
		state.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
		state.DepthStencilState.StencilEnable = false;
		state.DepthStencilState.StencilReadMask = D3D12_DEFAULT_STENCIL_READ_MASK;
		state.DepthStencilState.StencilWriteMask = D3D12_DEFAULT_STENCIL_WRITE_MASK;
		const D3D12_DEPTH_STENCILOP_DESC defaultStencilOp =
		{
			D3D12_STENCIL_OP_KEEP,
			D3D12_STENCIL_OP_KEEP,
			D3D12_STENCIL_OP_KEEP,
			D3D12_COMPARISON_FUNC_ALWAYS
		};
		state.DepthStencilState.FrontFace = defaultStencilOp;
		state.DepthStencilState.BackFace = defaultStencilOp;
		state.SampleMask = UINT_MAX;
		state.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		state.NumRenderTargets = 1;
		state.RTVFormats[0] = DXGI_FORMAT_B8G8R8A8_UNORM;
		state.DSVFormat = DXGI_FORMAT_D32_FLOAT;
		state.SampleDesc.Count = 1;
		check_hresult(d3dDevice->CreateGraphicsPipelineState(&state, IID_PPV_ARGS(&pipelineState)));
		check_hresult(d3dDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, commandAllocators[0].get(), pipelineState.get(), IID_PPV_ARGS(&commandList)));
		VertexPositionColor cubeVertices[] =
		{
			{ XMFLOAT3(-0.5f, -0.5f, -0.5f), XMFLOAT3(0.0f, 0.0f, 0.0f) },
			{ XMFLOAT3(-0.5f, -0.5f,  0.5f), XMFLOAT3(0.0f, 0.0f, 1.0f) },
			{ XMFLOAT3(-0.5f,  0.5f, -0.5f), XMFLOAT3(0.0f, 1.0f, 0.0f) },
			{ XMFLOAT3(-0.5f,  0.5f,  0.5f), XMFLOAT3(0.0f, 1.0f, 1.0f) },
			{ XMFLOAT3(0.5f, -0.5f, -0.5f), XMFLOAT3(1.0f, 0.0f, 0.0f) },
			{ XMFLOAT3(0.5f, -0.5f,  0.5f), XMFLOAT3(1.0f, 0.0f, 1.0f) },
			{ XMFLOAT3(0.5f,  0.5f, -0.5f), XMFLOAT3(1.0f, 1.0f, 0.0f) },
			{ XMFLOAT3(0.5f,  0.5f,  0.5f), XMFLOAT3(1.0f, 1.0f, 1.0f) }
		};
		auto vertexBufferSize = sizeof(cubeVertices);
		D3D12_HEAP_PROPERTIES defaultHeapProperties{ D3D12_HEAP_TYPE_DEFAULT };
		defaultHeapProperties.CreationNodeMask = 1;
		defaultHeapProperties.VisibleNodeMask = 1;
		D3D12_RESOURCE_DESC vertexBufferDesc{ D3D12_RESOURCE_DIMENSION_BUFFER };
		vertexBufferDesc.Width = vertexBufferSize;
		vertexBufferDesc.Height = 1;
		vertexBufferDesc.DepthOrArraySize = 1;
		vertexBufferDesc.MipLevels = 1;
		vertexBufferDesc.SampleDesc.Count = 1;
		vertexBufferDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
		check_hresult(d3dDevice->CreateCommittedResource(&defaultHeapProperties, D3D12_HEAP_FLAG_NONE, &vertexBufferDesc, D3D12_RESOURCE_STATE_COPY_DEST, nullptr, IID_PPV_ARGS(&vertexBuffer)));
		D3D12_HEAP_PROPERTIES uploadHeapProperties{ D3D12_HEAP_TYPE_UPLOAD };
		com_ptr<ID3D12Resource> vertexBufferUpload;
		check_hresult(d3dDevice->CreateCommittedResource(&uploadHeapProperties, D3D12_HEAP_FLAG_NONE, &vertexBufferDesc, D3D12_RESOURCE_STATE_GENERIC_READ, nullptr, IID_PPV_ARGS(&vertexBufferUpload)));
		D3D12_SUBRESOURCE_DATA vertexData{ cubeVertices, (LONG_PTR)vertexBufferSize, vertexData.RowPitch };
		UploadBufferToGPU(vertexData, vertexBufferSize, vertexBuffer, vertexBufferUpload, vertexBufferDesc);
		unsigned short cubeIndices[] =
		{
			0, 2, 1,
			1, 2, 3,
			4, 5, 6,
			5, 7, 6,
			0, 1, 5,
			0, 5, 4,
			2, 6, 7,
			2, 7, 3,
			0, 4, 6,
			0, 6, 2,
			1, 3, 7,
			1, 7, 5
		};
		auto indexBufferSize = sizeof(cubeIndices);
		D3D12_RESOURCE_DESC indexBufferDesc{ D3D12_RESOURCE_DIMENSION_BUFFER };
		indexBufferDesc.Width = indexBufferSize;
		indexBufferDesc.Height = 1;
		indexBufferDesc.DepthOrArraySize = 1;
		indexBufferDesc.MipLevels = 1;
		indexBufferDesc.SampleDesc.Count = 1;
		indexBufferDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
		check_hresult(d3dDevice->CreateCommittedResource(&defaultHeapProperties, D3D12_HEAP_FLAG_NONE, &indexBufferDesc, D3D12_RESOURCE_STATE_COPY_DEST, nullptr, IID_PPV_ARGS(&indexBuffer)));
		com_ptr<ID3D12Resource> indexBufferUpload;
		check_hresult(d3dDevice->CreateCommittedResource(&uploadHeapProperties, D3D12_HEAP_FLAG_NONE, &indexBufferDesc, D3D12_RESOURCE_STATE_GENERIC_READ, nullptr, IID_PPV_ARGS(&indexBufferUpload)));
		D3D12_SUBRESOURCE_DATA indexData{ cubeIndices, (LONG_PTR)indexBufferSize, indexData.RowPitch };
		UploadBufferToGPU(indexData, indexBufferSize, indexBuffer, indexBufferUpload, indexBufferDesc);
		D3D12_DESCRIPTOR_HEAP_DESC heapDesc{ };
		heapDesc.NumDescriptors = frameCount;
		heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
		heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
		check_hresult(d3dDevice->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(&cbvHeap)));
		D3D12_RESOURCE_DESC constantBufferDesc{ D3D12_RESOURCE_DIMENSION_BUFFER };
		constantBufferDesc.Width = frameCount * alignedConstantBufferSize;
		constantBufferDesc.Height = 1;
		constantBufferDesc.DepthOrArraySize = 1;
		constantBufferDesc.MipLevels = 1;
		constantBufferDesc.SampleDesc.Count = 1;
		constantBufferDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
		check_hresult(d3dDevice->CreateCommittedResource(&uploadHeapProperties, D3D12_HEAP_FLAG_NONE, &constantBufferDesc, D3D12_RESOURCE_STATE_GENERIC_READ, nullptr, IID_PPV_ARGS(&constantBuffer)));
		D3D12_GPU_VIRTUAL_ADDRESS cbvGpuAddress = constantBuffer->GetGPUVirtualAddress();
		D3D12_CPU_DESCRIPTOR_HANDLE cbvCpuHandle(cbvHeap->GetCPUDescriptorHandleForHeapStart());
		cbvDescriptorSize = d3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
		for (auto i = 0; i < frameCount; i++)
		{
			D3D12_CONSTANT_BUFFER_VIEW_DESC desc;
			desc.BufferLocation = cbvGpuAddress;
			desc.SizeInBytes = alignedConstantBufferSize;
			d3dDevice->CreateConstantBufferView(&desc, cbvCpuHandle);
			cbvGpuAddress += desc.SizeInBytes;
			cbvCpuHandle.ptr += cbvDescriptorSize;
		}
		D3D12_RANGE readRange{ };
		check_hresult(constantBuffer->Map(0, &readRange, reinterpret_cast<void**>(&mappedConstantBuffer)));
		memset(mappedConstantBuffer, 0, frameCount * alignedConstantBufferSize);
		check_hresult(commandList->Close());
		ID3D12CommandList* ppCommandLists[] = { commandList.get() };
		commandQueue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);
		vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
		vertexBufferView.StrideInBytes = sizeof(VertexPositionColor);
		vertexBufferView.SizeInBytes = sizeof(cubeVertices);
		indexBufferView.BufferLocation = indexBuffer->GetGPUVirtualAddress();
		indexBufferView.SizeInBytes = sizeof(cubeIndices);
		indexBufferView.Format = DXGI_FORMAT_R16_UINT;
		WaitForGPU(0, "WaitForGPU failed while creating device");
		UpdateMatrices(outputSize, orientationTransform3D);
		deviceRemoved = false;
	}

	void MainPage::RemoveDevice()
	{
		deviceRemoved = true;
		swapChainPanel().Dispatcher().RunAsync(CoreDispatcherPriority::High, [this]()
			{
				com_ptr<ISwapChainPanelNative> panelNative;
				swapChainPanel().as(panelNative);
				check_hresult(panelNative->SetSwapChain(nullptr));
			});
	}

	void MainPage::swapChainPanel_SizeChanged(IInspectable const&, SizeChangedEventArgs const& e)
	{
		if (deviceRemoved)
		{
			return;
		}
		std::lock_guard lock(renderLoopMutex);
		WaitForGPU(currentFrame, "WaitForGPU failed while resizing swap chain panel");
		CleanupFrameData();
		auto displayInformation = DisplayInformation::GetForCurrentView();
		Size outputSize;
		CreateOutputSize(e.NewSize().Width, e.NewSize().Height, displayInformation, outputSize);
		DXGI_MODE_ROTATION rotation = DXGI_MODE_ROTATION_UNSPECIFIED;
		CreateRotation(displayInformation, rotation);
		Size renderTargetSize;
		CreateRenderTargetSize(rotation, outputSize, renderTargetSize);
		auto result = swapChain->ResizeBuffers(frameCount, lround(renderTargetSize.Width), lround(renderTargetSize.Height), DXGI_FORMAT_B8G8R8A8_UNORM, 0);
		if (result == DXGI_ERROR_DEVICE_REMOVED || result == DXGI_ERROR_DEVICE_RESET)
		{
			RemoveDevice();
			return;
		}
		check_hresult(result);
		XMFLOAT4X4 orientationTransform3D;
		CreateOrientationTransform3D(rotation, orientationTransform3D);
		UpdateSwapChainTransform(rotation, swapChainPanel().CompositionScaleX(), swapChainPanel().CompositionScaleY());
		currentFrame = swapChain->GetCurrentBackBufferIndex();
		CreateRenderTargetViews();
		depthStencil = nullptr;
		CreateDepthStencilBuffer(lround(renderTargetSize.Width), lround(renderTargetSize.Height));
		viewport = { 0, 0, renderTargetSize.Width, renderTargetSize.Height, 0, 1 };
		UpdateMatrices(outputSize, orientationTransform3D);
	}

	void MainPage::swapChainPanel_CompositionScaleChanged(SwapChainPanel const& sender, IInspectable const&)
	{
		if (deviceRemoved)
		{
			return;
		}
		std::lock_guard lock(renderLoopMutex);
		WaitForGPU(currentFrame, "WaitForGPU failed while setting composition scale");
		CleanupFrameData();
		auto displayInformation = DisplayInformation::GetForCurrentView();
		Size outputSize;
		CreateOutputSize((float)swapChainPanel().ActualWidth(), (float)swapChainPanel().ActualHeight(), displayInformation, outputSize);
		DXGI_MODE_ROTATION rotation = DXGI_MODE_ROTATION_UNSPECIFIED;
		CreateRotation(displayInformation, rotation);
		Size renderTargetSize;
		CreateRenderTargetSize(rotation, outputSize, renderTargetSize);
		auto result = swapChain->ResizeBuffers(frameCount, lround(renderTargetSize.Width), lround(renderTargetSize.Height), DXGI_FORMAT_B8G8R8A8_UNORM, 0);
		if (result == DXGI_ERROR_DEVICE_REMOVED || result == DXGI_ERROR_DEVICE_RESET)
		{
			RemoveDevice();
			return;
		}
		check_hresult(result);
		XMFLOAT4X4 orientationTransform3D;
		CreateOrientationTransform3D(rotation, orientationTransform3D);
		UpdateSwapChainTransform(rotation, sender.CompositionScaleX(), sender.CompositionScaleY());
		currentFrame = swapChain->GetCurrentBackBufferIndex();
		CreateRenderTargetViews();
		depthStencil = nullptr;
		CreateDepthStencilBuffer(lround(renderTargetSize.Width), lround(renderTargetSize.Height));
		viewport = { 0, 0, renderTargetSize.Width, renderTargetSize.Height, 0, 1 };
		UpdateMatrices(outputSize, orientationTransform3D);
	}

	void MainPage::DpiChanged(Windows::Graphics::Display::DisplayInformation const& sender, IInspectable const&)
	{
		if (deviceRemoved)
		{
			return;
		}
		std::lock_guard lock(renderLoopMutex);
		WaitForGPU(currentFrame, "WaitForGPU failed while setting screen DPI");
		CleanupFrameData();
		Size outputSize;
		CreateOutputSize((float)swapChainPanel().ActualWidth(), (float)swapChainPanel().ActualHeight(), sender, outputSize);
		DXGI_MODE_ROTATION rotation = DXGI_MODE_ROTATION_UNSPECIFIED;
		CreateRotation(sender, rotation);
		Size renderTargetSize;
		CreateRenderTargetSize(rotation, outputSize, renderTargetSize);
		auto result = swapChain->ResizeBuffers(frameCount, lround(renderTargetSize.Width), lround(renderTargetSize.Height), DXGI_FORMAT_B8G8R8A8_UNORM, 0);
		if (result == DXGI_ERROR_DEVICE_REMOVED || result == DXGI_ERROR_DEVICE_RESET)
		{
			RemoveDevice();
			return;
		}
		check_hresult(result);
		XMFLOAT4X4 orientationTransform3D;
		CreateOrientationTransform3D(rotation, orientationTransform3D);
		UpdateSwapChainTransform(rotation, swapChainPanel().CompositionScaleX(), swapChainPanel().CompositionScaleX());
		currentFrame = swapChain->GetCurrentBackBufferIndex();
		CreateRenderTargetViews();
		depthStencil = nullptr;
		CreateDepthStencilBuffer(lround(renderTargetSize.Width), lround(renderTargetSize.Height));
		viewport = { 0, 0, renderTargetSize.Width, renderTargetSize.Height, 0, 1 };
		UpdateMatrices(outputSize, orientationTransform3D);
	}

	void MainPage::OrientationChanged(Windows::Graphics::Display::DisplayInformation const& sender, IInspectable const&)
	{
		if (deviceRemoved)
		{
			return;
		}
		std::lock_guard lock(renderLoopMutex);
		WaitForGPU(currentFrame, "WaitForGPU failed while setting screen orientation");
		CleanupFrameData();
		Size outputSize;
		CreateOutputSize((float)swapChainPanel().ActualWidth(), (float)swapChainPanel().ActualHeight(), sender, outputSize);
		DXGI_MODE_ROTATION rotation = DXGI_MODE_ROTATION_UNSPECIFIED;
		CreateRotation(sender, rotation);
		Size renderTargetSize;
		CreateRenderTargetSize(rotation, outputSize, renderTargetSize);
		auto result = swapChain->ResizeBuffers(frameCount, lround(renderTargetSize.Width), lround(renderTargetSize.Height), DXGI_FORMAT_B8G8R8A8_UNORM, 0);
		if (result == DXGI_ERROR_DEVICE_REMOVED || result == DXGI_ERROR_DEVICE_RESET)
		{
			RemoveDevice();
			return;
		}
		check_hresult(result);
		XMFLOAT4X4 orientationTransform3D;
		CreateOrientationTransform3D(rotation, orientationTransform3D);
		UpdateSwapChainTransform(rotation, swapChainPanel().CompositionScaleX(), swapChainPanel().CompositionScaleX());
		currentFrame = swapChain->GetCurrentBackBufferIndex();
		CreateRenderTargetViews();
		depthStencil = nullptr;
		CreateDepthStencilBuffer(lround(renderTargetSize.Width), lround(renderTargetSize.Height));
		viewport = { 0, 0, renderTargetSize.Width, renderTargetSize.Height, 0, 1 };
		UpdateMatrices(outputSize, orientationTransform3D);
	}

	void MainPage::DisplayContentsInvalidated(Windows::Graphics::Display::DisplayInformation const&, IInspectable const&)
	{
		if (deviceRemoved)
		{
			return;
		}
		std::lock_guard lock(renderLoopMutex);
		if (FAILED(d3dDevice->GetDeviceRemovedReason()))
		{
			RemoveDevice();
			return;
		}
		DXGI_ADAPTER_DESC previousDesc;
		com_ptr<IDXGIAdapter1> previousDefaultAdapter;
		check_hresult(dxgiFactory->EnumAdapters1(0, previousDefaultAdapter.put()));
		check_hresult(previousDefaultAdapter->GetDesc(&previousDesc));
		DXGI_ADAPTER_DESC currentDesc;
		com_ptr<IDXGIFactory4> currentDxgiFactory;
		check_hresult(CreateDXGIFactory1(IID_PPV_ARGS(&currentDxgiFactory)));
		com_ptr<IDXGIAdapter1> currentDefaultAdapter;
		check_hresult(currentDxgiFactory->EnumAdapters1(0, currentDefaultAdapter.put()));
		check_hresult(currentDefaultAdapter->GetDesc(&currentDesc));
		if (previousDesc.AdapterLuid.LowPart != currentDesc.AdapterLuid.LowPart || previousDesc.AdapterLuid.HighPart != currentDesc.AdapterLuid.HighPart)
		{
			RemoveDevice();
		}
	}

	void MainPage::RenderLoop(IAsyncAction const& action)
	{
		if (action.Status() != AsyncStatus::Started)
		{
			return;
		}
		if (deviceRemoved)
		{
#if defined(_DEBUG)
			com_ptr<IDXGIDebug1> dxgiDebug;
			if (SUCCEEDED(DXGIGetDebugInterface1(0, IID_PPV_ARGS(&dxgiDebug))))
			{
				dxgiDebug->ReportLiveObjects(DXGI_DEBUG_ALL, DXGI_DEBUG_RLO_FLAGS(DXGI_DEBUG_RLO_SUMMARY | DXGI_DEBUG_RLO_IGNORE_INTERNAL));
			}
#endif
			CreateDevice();
		}
		else
		{
			std::lock_guard lock(renderLoopMutex);
			PIXBeginEvent(commandQueue.get(), 0, L"Update");
			auto updated = Update();
			PIXEndEvent(commandQueue.get());
			if (updated)
			{
				PIXBeginEvent(commandQueue.get(), 0, L"Render");
				Render();
				PIXEndEvent(commandQueue.get());
			}
		}
		renderLoopWorker = ThreadPool::RunAsync([=](IAsyncAction const& action)
			{
				RenderLoop(action);
			}, WorkItemPriority::High, WorkItemOptions::TimeSliced);
	}

	bool MainPage::Update()
	{
		if (deviceRemoved)
		{
			return false;
		}
		if (previousTime.QuadPart == 0)
		{
			QueryPerformanceCounter(&previousTime);
			return false;
		}
		LARGE_INTEGER time;
		QueryPerformanceCounter(&time);
		LARGE_INTEGER frequency;
		QueryPerformanceFrequency(&frequency);
		auto elapsed = (float)(time.QuadPart - previousTime.QuadPart) / (float)frequency.QuadPart;
		angle += elapsed * XM_PIDIV4;
		while (angle > XM_2PI)
		{
			angle -= XM_2PI;
		}
		XMStoreFloat4x4(&constantBufferData.model, XMMatrixTranspose(XMMatrixRotationY(angle)));
		UINT8* destination = mappedConstantBuffer + currentFrame * alignedConstantBufferSize;
		memcpy(destination, &constantBufferData, sizeof(constantBufferData));
		previousTime = time;
		return true;
	}

	void MainPage::Render()
	{
		check_hresult(commandAllocators[currentFrame]->Reset());
		check_hresult(commandList->Reset(commandAllocators[currentFrame].get(), pipelineState.get()));
		PIXBeginEvent(commandList.get(), 0, L"Draw the cube");
		commandList->SetGraphicsRootSignature(rootSignature.get());
		ID3D12DescriptorHeap* ppHeaps[] = { cbvHeap.get() };
		commandList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);
		D3D12_GPU_DESCRIPTOR_HANDLE gpuHandle{ cbvHeap->GetGPUDescriptorHandleForHeapStart().ptr + currentFrame * cbvDescriptorSize };
		commandList->SetGraphicsRootDescriptorTable(0, gpuHandle);
		commandList->RSSetViewports(1, &viewport);
		commandList->RSSetScissorRects(1, &scissorRect);
		D3D12_RESOURCE_BARRIER renderTargetResourceBarrier{ };
		renderTargetResourceBarrier.Transition.pResource = renderTargets[currentFrame].get();
		renderTargetResourceBarrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
		renderTargetResourceBarrier.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;
		renderTargetResourceBarrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
		commandList->ResourceBarrier(1, &renderTargetResourceBarrier);
		D3D12_CPU_DESCRIPTOR_HANDLE renderTargetView{ rtvHeap->GetCPUDescriptorHandleForHeapStart().ptr + currentFrame * rtvDescriptorSize };
		D3D12_CPU_DESCRIPTOR_HANDLE depthStencilView{ dsvHeap->GetCPUDescriptorHandleForHeapStart().ptr };
		commandList->ClearRenderTargetView(renderTargetView, DirectX::Colors::CornflowerBlue, 0, nullptr);
		commandList->ClearDepthStencilView(depthStencilView, D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);
		commandList->OMSetRenderTargets(1, &renderTargetView, false, &depthStencilView);
		commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		commandList->IASetVertexBuffers(0, 1, &vertexBufferView);
		commandList->IASetIndexBuffer(&indexBufferView);
		commandList->DrawIndexedInstanced(36, 1, 0, 0, 0);
		D3D12_RESOURCE_BARRIER presentResourceBarrier{ };
		presentResourceBarrier.Transition.pResource = renderTargets[currentFrame].get();
		presentResourceBarrier.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
		presentResourceBarrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;
		presentResourceBarrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
		commandList->ResourceBarrier(1, &presentResourceBarrier);
		PIXEndEvent(commandList.get());
		check_hresult(commandList->Close());
		ID3D12CommandList* ppCommandLists[] = { commandList.get() };
		commandQueue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);
		auto result = swapChain->Present(1, 0);
		if (result == DXGI_ERROR_DEVICE_REMOVED || result == DXGI_ERROR_DEVICE_RESET)
		{
			RemoveDevice();
			return;
		}
		check_hresult(result);
		auto currentFenceValue = fenceValues[currentFrame];
		check_hresult(commandQueue->Signal(fence.get(), currentFenceValue));
		currentFrame = swapChain->GetCurrentBackBufferIndex();
		if (fence->GetCompletedValue() < fenceValues[currentFrame])
		{
			check_hresult(fence->SetEventOnCompletion(fenceValues[currentFrame], fenceEvent));
			if (WaitForSingleObjectEx(fenceEvent, 5000, FALSE) != WAIT_OBJECT_0)
			{
				throw std::runtime_error("WaitForSingleObjectEx failed at rendering stage.");
			}
		}
		fenceValues[currentFrame] = currentFenceValue + 1;
	}

	void MainPage::WaitForGPU(int frame, const char* timeoutError)
	{
		check_hresult(commandQueue->Signal(fence.get(), fenceValues[frame]));
		check_hresult(fence->SetEventOnCompletion(fenceValues[frame], fenceEvent));
		if (WaitForSingleObjectEx(fenceEvent, 5000, FALSE) != WAIT_OBJECT_0)
		{
			throw std::runtime_error(timeoutError);
		}
		fenceValues[frame]++;
	}

	void MainPage::UploadBufferToGPU(D3D12_SUBRESOURCE_DATA data, size_t size, com_ptr<ID3D12Resource> const& buffer, com_ptr<ID3D12Resource> const& bufferUpload, D3D12_RESOURCE_DESC const& descriptor)
	{
		UINT64 MemToAlloc = static_cast<UINT64>(sizeof(D3D12_PLACED_SUBRESOURCE_FOOTPRINT) + sizeof(UINT) + sizeof(UINT64));
		if (MemToAlloc > SIZE_MAX)
		{
			throw std::bad_alloc();
		}
		void* pMem = HeapAlloc(GetProcessHeap(), 0, static_cast<SIZE_T>(MemToAlloc));
		if (pMem == NULL)
		{
			throw std::bad_alloc();
		}
		D3D12_PLACED_SUBRESOURCE_FOOTPRINT* pLayouts = reinterpret_cast<D3D12_PLACED_SUBRESOURCE_FOOTPRINT*>(pMem);
		auto pRowSizesInBytes = (UINT64*)(pLayouts + 1);
		auto pNumRows = (UINT*)(pRowSizesInBytes + 1);
		UINT64 RequiredSize = 0;
		d3dDevice->GetCopyableFootprints(&descriptor, 0, 1, 0, pLayouts, pNumRows, pRowSizesInBytes, &RequiredSize);
		if (size < (size_t)(RequiredSize + pLayouts[0].Offset) || RequiredSize >(SIZE_T) - 1)
		{
			throw std::bad_alloc();
		}
		BYTE* pData = nullptr;
		check_hresult(bufferUpload->Map(0, NULL, reinterpret_cast<void**>(&pData)));
		for (auto i = 0; i < 1; i++)
		{
			if (pRowSizesInBytes[i] > (SIZE_T)-1)
			{
				throw std::bad_alloc();
			}
			D3D12_MEMCPY_DEST DestData{ pData + pLayouts[i].Offset, pLayouts[i].Footprint.RowPitch, pLayouts[i].Footprint.RowPitch * pNumRows[i] };
			for (UINT j = 0; j < pLayouts[i].Footprint.Depth; j++)
			{
				BYTE* pDestSlice = reinterpret_cast<BYTE*>(DestData.pData) + DestData.SlicePitch * j;
				const BYTE* pSrcSlice = reinterpret_cast<const BYTE*>(data.pData) + data.SlicePitch * j;
				for (UINT k = 0; k < pNumRows[i]; k++)
				{
					memcpy(pDestSlice + DestData.RowPitch * k, pSrcSlice + data.RowPitch * k, pRowSizesInBytes[i]);
				}
			}
		}
		bufferUpload->Unmap(0, NULL);
		D3D12_BOX SrcBox;
		SrcBox.left = UINT(pLayouts[0].Offset);
		SrcBox.top = 0;
		SrcBox.front = 0;
		SrcBox.right = UINT(pLayouts[0].Offset + pLayouts[0].Footprint.Width);
		SrcBox.bottom = 1;
		SrcBox.back = 1;
		commandList->CopyBufferRegion(buffer.get(), 0, bufferUpload.get(), pLayouts[0].Offset, pLayouts[0].Footprint.Width);
		HeapFree(GetProcessHeap(), 0, pMem);
		D3D12_RESOURCE_BARRIER transition{ };
		transition.Transition.pResource = buffer.get();
		transition.Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
		transition.Transition.StateAfter = D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER;
		transition.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
		commandList->ResourceBarrier(1, &transition);
	}

	void MainPage::CleanupFrameData()
	{
		for (auto i = 0; i < frameCount; i++)
		{
			renderTargets[i] = nullptr;
			fenceValues[i] = fenceValues[currentFrame];
		}
	}

	void MainPage::CreateOutputSize(float width, float height, DisplayInformation const& displayInformation, Size& outputSize)
	{
		width = max(1, floor(width * displayInformation.LogicalDpi() / 96 + 0.5f));
		height = max(1, floor(height * displayInformation.LogicalDpi() / 96 + 0.5f));
		outputSize.Width = width;
		outputSize.Height = height;
	}

	void MainPage::CreateRotation(DisplayInformation const& displayInformation, DXGI_MODE_ROTATION& rotation)
	{
		switch (displayInformation.NativeOrientation())
		{
		case DisplayOrientations::Landscape:
			switch (displayInformation.CurrentOrientation())
			{
			case DisplayOrientations::Landscape:
				rotation = DXGI_MODE_ROTATION_IDENTITY;
				break;

			case DisplayOrientations::Portrait:
				rotation = DXGI_MODE_ROTATION_ROTATE270;
				break;

			case DisplayOrientations::LandscapeFlipped:
				rotation = DXGI_MODE_ROTATION_ROTATE180;
				break;

			case DisplayOrientations::PortraitFlipped:
				rotation = DXGI_MODE_ROTATION_ROTATE90;
				break;
			}
			break;
		case DisplayOrientations::Portrait:
			switch (displayInformation.CurrentOrientation())
			{
			case DisplayOrientations::Landscape:
				rotation = DXGI_MODE_ROTATION_ROTATE90;
				break;

			case DisplayOrientations::Portrait:
				rotation = DXGI_MODE_ROTATION_IDENTITY;
				break;

			case DisplayOrientations::LandscapeFlipped:
				rotation = DXGI_MODE_ROTATION_ROTATE270;
				break;

			case DisplayOrientations::PortraitFlipped:
				rotation = DXGI_MODE_ROTATION_ROTATE180;
				break;
			}
			break;
		}
	}

	void MainPage::CreateRenderTargetSize(DXGI_MODE_ROTATION& rotation, Size const& size, Size& renderTargetSize)
	{
		if (rotation == DXGI_MODE_ROTATION_ROTATE90 || rotation == DXGI_MODE_ROTATION_ROTATE270)
		{
			renderTargetSize.Width = size.Height;
			renderTargetSize.Height = size.Width;
		}
		else
		{
			renderTargetSize.Width = size.Width;
			renderTargetSize.Height = size.Height;
		}
	}

	void MainPage::CreateOrientationTransform3D(DXGI_MODE_ROTATION rotation, XMFLOAT4X4& orientationTransform3D)
	{
		switch (rotation)
		{
		case DXGI_MODE_ROTATION_IDENTITY:
			orientationTransform3D = Rotation0;
			break;

		case DXGI_MODE_ROTATION_ROTATE90:
			orientationTransform3D = Rotation270;
			break;

		case DXGI_MODE_ROTATION_ROTATE180:
			orientationTransform3D = Rotation180;
			break;

		case DXGI_MODE_ROTATION_ROTATE270:
			orientationTransform3D = Rotation90;
			break;

		default:
			throw std::runtime_error("Unknown rotation mode.");
		}
	}

	void MainPage::UpdateSwapChainTransform(DXGI_MODE_ROTATION rotation, float compositionScaleX, float compositionScaleY)
	{
		check_hresult(swapChain->SetRotation(rotation));
		DXGI_MATRIX_3X2_F inverseScale{ };
		inverseScale._11 = 1.0f / compositionScaleX;
		inverseScale._22 = 1.0f / compositionScaleY;
		check_hresult(swapChain->SetMatrixTransform(&inverseScale));
	}

	void MainPage::CreateRenderTargetViews()
	{
		D3D12_CPU_DESCRIPTOR_HANDLE rtvDescriptor{ rtvHeap->GetCPUDescriptorHandleForHeapStart() };
		for (auto i = 0; i < frameCount; i++)
		{
			check_hresult(swapChain->GetBuffer(i, IID_PPV_ARGS(&renderTargets[i])));
			d3dDevice->CreateRenderTargetView(renderTargets[i].get(), nullptr, rtvDescriptor);
			rtvDescriptor.ptr += rtvDescriptorSize;
		}
	}

	void MainPage::CreateDepthStencilBuffer(UINT width, UINT height)
	{
		D3D12_HEAP_PROPERTIES depthHeapProperties{ D3D12_HEAP_TYPE_DEFAULT };
		depthHeapProperties.CreationNodeMask = 1;
		depthHeapProperties.VisibleNodeMask = 1;
		D3D12_RESOURCE_DESC depthResourceDesc{ D3D12_RESOURCE_DIMENSION_TEXTURE2D };
		depthResourceDesc.Width = width;
		depthResourceDesc.Height = height;
		depthResourceDesc.DepthOrArraySize = 1;
		depthResourceDesc.Format = DXGI_FORMAT_D32_FLOAT;
		depthResourceDesc.SampleDesc.Count = 1;
		depthResourceDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;
		D3D12_CLEAR_VALUE depthOptimizedClearValue{ DXGI_FORMAT_D32_FLOAT };
		depthOptimizedClearValue.DepthStencil.Depth = 1;
		check_hresult(d3dDevice->CreateCommittedResource(&depthHeapProperties, D3D12_HEAP_FLAG_NONE, &depthResourceDesc, D3D12_RESOURCE_STATE_DEPTH_WRITE, &depthOptimizedClearValue, IID_PPV_ARGS(&depthStencil)));
		D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc{ DXGI_FORMAT_D32_FLOAT, D3D12_DSV_DIMENSION_TEXTURE2D };
		d3dDevice->CreateDepthStencilView(depthStencil.get(), &dsvDesc, dsvHeap->GetCPUDescriptorHandleForHeapStart());
	}

	void MainPage::UpdateMatrices(Size const& outputSize, XMFLOAT4X4 const& orientationTransform3D)
	{
		auto aspectRatio = outputSize.Width / outputSize.Height;
		auto fovAngleY = 70.0f * XM_PI / 180;
		scissorRect = { 0, 0, (LONG)viewport.Width, (LONG)viewport.Height };
		if (aspectRatio < 1)
		{
			fovAngleY *= 2;
		}
		auto perspectiveMatrix = XMMatrixPerspectiveFovRH(fovAngleY, aspectRatio, 0.01f, 100);
		auto orientationMatrix = XMLoadFloat4x4(&orientationTransform3D);
		XMStoreFloat4x4(&constantBufferData.projection, XMMatrixTranspose(perspectiveMatrix * orientationMatrix));
		static const XMVECTORF32 eye{ 0, 0.7f, 1.5f, 0 };
		static const XMVECTORF32 at{ 0, -0.1f, 0, 0 };
		static const XMVECTORF32 up{ 0, 1, 0, 0 };
		XMStoreFloat4x4(&constantBufferData.view, XMMatrixTranspose(XMMatrixLookAtRH(eye, at, up)));
	}
}
